## Installation

1. Install VirtualBox (https://www.virtualbox.org/)
2. Install Vagrant (https://www.vagrantup.com/)
3. Open up a terminal or a command line tool
4. Install the Vagrant plugin "vagrant-triggers" with the command: "vagrant plugin install vagrant-triggers"
5. Install the Vagrant plugin "vagrant-winnfsd" with the command: "vagrant plugin install vagrant-winnfsd"
6. Start the Vagrant box with the command: "vagrant up"
7. Destroy the Vagrant box with the command: "vagrant destroy"

Submodules aktualisieren: git submodule update --remote --merge
git pull && git submodule init && git submodule update && git submodule status

echo '10.0.0.5  my.dev' | sudo tee -a /etc/hosts

PHP-Variable:
$_SERVER['VM_BOX']
$_SERVER['VM_ENVIRONMENT']