# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|

    # Load json config file
    vm_config_path = File.expand_path(File.dirname(__FILE__)) + "/puppet/config/config.json"
    vm_user_config_path = File.expand_path(File.dirname(__FILE__)) + "/puppet/config/vm.config.json"
    
    vm_config = JSON.parse(File.read(vm_config_path))
    vm_userconfig = JSON.parse(File.read(vm_user_config_path))

    # ssh forwarding
    config.ssh.forward_agent = true

    # Base box
    config.vm.box = "ubuntu/precise32"

    # Hostname
    config.vm.hostname = vm_userconfig["hostname"]

    # Private network ip
    config.vm.network :private_network, ip: vm_config["ip"]

    # Custom provider
    config.vm.provider "virtualbox" do |vb|

        # Give VM 1/4 system memory & access to all cpu cores on the host
        host = RbConfig::CONFIG['host_os']
        if host =~ /darwin/
            cpus = `sysctl -n hw.ncpu`.to_i
            # sysctl returns Bytes and we need to convert to MB
            mem = `sysctl -n hw.memsize`.to_i / 1024 / 1024 / 4
        elsif host =~ /linux/
            cpus = `nproc`.to_i
            # meminfo shows KB and we need to convert to MB
            mem = `grep 'MemTotal' /proc/meminfo | sed -e 's/MemTotal://' -e 's/ kB//'`.to_i / 1024 / 4
        else # sorry Windows folks, I can't help you
            cpus = 1
            mem = 2048
        end

        # Set cpu and memory
        vb.memory = "#{mem}"
        vb.cpus = "#{cpus}"
        
        vb.name = vm_userconfig["hostname"]
        
        # Set additional virtualbox settings
        vb.customize ["modifyvm", :id, "--chipset", "ich9"]
        vb.customize ["modifyvm", :id, "--pae", "on"]
        vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
        vb.customize ["modifyvm", :id, "--natdnsproxy1", "on"]

    end

    # Synched folders
    config.vm.synced_folder "./", "/vagrant", disabled: true
    vm_config["synced_folders"].each do |set_synced_folder|

        config.vm.synced_folder set_synced_folder["host_path"],
        set_synced_folder["guest_path"],
        type: set_synced_folder["type"] ? set_synced_folder["type"] : "rsync",
        mount_options: set_synced_folder["mount_options"] ? set_synced_folder["mount_options"] : []

    end

    # update puppet
    config.vm.provision :shell, :path => "puppet/scripts/upgrade_puppet.sh"
    
    # init puppet
    config.vm.provision "puppet" do |puppet|

        puppet.manifests_path = ["vm", "/vagrant/puppet/manifests"]
        puppet.manifest_file  = "init.pp"
        puppet.options = "--hiera_config /etc/hiera.yaml"
        puppet.module_path = "puppet/modules"
        puppet.facter = {
            "vm_config" => vm_config_path,
            "vm_userconfig" => vm_user_config_path
        }

    end

    # Triggers for additional puppet manifests
    if Vagrant.has_plugin?("vagrant-triggers")

        # Trigger after up
        config.trigger.after :up, :stdout => true, :force => true do
            info "Run up puppet manifest"
            run_remote "puppet apply --modulepath /vagrant/puppet/modules /vagrant/puppet/manifests/up.pp"
        end

        # Trigger before halt
        config.trigger.before :halt, :stdout => true, :force => true do
            info "Run destroy puppet manifest"
            run_remote "puppet apply --modulepath /vagrant/puppet/modules /vagrant/puppet/manifests/halt.pp"
        end

        # Trigger before destroy
        config.trigger.before :destroy, :stdout => true, :force => true do
            info "Run destroy puppet manifest"
            run_remote "puppet apply --modulepath /vagrant/puppet/modules /vagrant/puppet/manifests/destroy.pp"
        end

    end

end