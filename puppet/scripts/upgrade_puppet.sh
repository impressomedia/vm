#!/bin/bash

sed -i 's/^mesg n$/tty -s \&\& mesg n/g' /root/.profile

apt-get install --yes lsb-release > /dev/null
DISTRIB_CODENAME=$(lsb_release --codename --short)
DEB="puppetlabs-release-$DISTRIB_CODENAME.deb"
#DEB_PROVIDES="/etc/apt/sources.list.d/puppetlabs.list" # Assume that this file's existence means we have the Puppet Labs repo added

#if [ ! -e $DEB_PROVIDES ]
#then
    # Print statement useful for debugging, but automated runs of this will interpret any output as an error
    # print "Could not find $DEB_PROVIDES - fetching and installing $DEB"
    wget -q http://apt.puppetlabs.com/$DEB
    sudo dpkg -i $DEB
#fi

sudo add-apt-repository ppa:ondrej/php5 > /dev/null

sudo apt-get update > /dev/null
sudo apt-get install --yes php5 puppet > /dev/null

echo 'PHP & Puppet up to date.'

sudo sed -i /etc/default/puppet -e 's/START=no/START=yes/'
sudo ln -s /vagrant/puppet/modules /usr/share/puppet/modules
#sudo service puppet start