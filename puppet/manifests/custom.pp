Exec {
  path => ['/bin/', '/sbin/' , '/usr/bin/', '/usr/sbin/']
}
node default {
  #include apt

  exec { 'apt-get update':
    command => 'apt-get update',
    timeout => 60,
    tries   => 3
  }
  
  class { 'apt':
    force_aptget_update => true
  }
  
  #class { 'apt':
  #  always_apt_update    => false,
  #  apt_update_frequency => undef,
  #  disable_keys         => undef,
  #  proxy_host           => false,
  #  proxy_port           => '8080',
  #  purge_sources_list   => false,
  #  purge_sources_list_d => false,
  #  purge_preferences_d  => false,
  #  update_timeout       => undef,
  #  fancy_progress       => undef
  #}
  
  package { ['python-software-properties']:
    ensure  => 'installed',
    require => Exec['apt-get update'],
    before => Package[['apache', 'php']]
  }
  
  package { ['build-essential', 'git', 'curl']:
    ensure  => 'installed',
    require => Exec['apt-get update'],
    before => Package[['apache', 'php']]
  }
  
  class { 'apache': }
  
  #$vmconfig = parsejson(file($::vm_config))
  #$vmuserconfig = parsejson(file($::vm_userconfig))
  
  #notify { $vmconfig: }
  #notify { $vmuserconfig: }
  
  #$lclconfig = parsejson(file('/vagrant/www/vm.config.json'))
  #notify { $::vm_userconfig: }
  
  file { '/var/www/public':
    ensure  => 'link',
    target  => '/vagrant/www',
    require => Package['apache'],
    notify  => Service['apache']
  }
  
  apache::module { 'rewrite':
    require => Package['apache']
  }
  
  #lets read the config files
  
  #$apacheenvvars = split($::apache_env_vars, ",")
  #$serveraliases = split($::serveraliases, ",")
  #$phpmodules = split($::phpmodules, ",")
  
  apache::vhost { 'public':
    docroot                   => '/var/www/public',
    server_name               => $::hostname,
    serveraliases             => ["city2be.net"],
    server_admin              => "",
    directory                 => '/var/www/public',
    directory_options         => 'Indexes FollowSymLinks MultiViews',
    directory_allow_override  => 'All',
    template                  => '/vagrant/puppet/modules/apache/templates/virtualhost/vhost.conf.erb',
    require                   => Package[['apache']],
    env_variables             => [],
  }
  
  include apache::ssl
  
  #apt::ppa { 'ppa:ondrej/php5-oldstable':
  #  before  => Class['php'],
  #}
  
  class { 'php': }
  
  php::module { ['curl', 'gd', 'imagick', 'mysql', 'cli', 'intl', 'mcrypt']:
    require => Package['php']
  }
  php::ini { 'php':
    value   => ['date.timezone = Europe/Berlin', 'display_errors = On', 'error_reporting = 6143', 'memory_limit = 1024M', 'max_execution_time = 1000', 'max_input_time = 1000', 'upload_max_filesize = 100M', 'post_max_size = 100M', 'realpath_cache_size = 1024KB', 'upload_tmp_dir = /var/www/tmp'],
    target  => 'php.ini',
    require => Package['php'],
    service => 'apache'
  }
  
  exec { 'usermod www-data':
    command => 'sudo usermod -a -G www-data vagrant'
  }
  
  exec { 'usermod sudo':
    command => 'sudo usermod -a -G sudo vagrant'
  }
  
  class { 'postfix': }

}