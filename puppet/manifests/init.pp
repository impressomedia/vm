Exec {
  path => ['/bin/', '/sbin/' , '/usr/bin/', '/usr/sbin/']
}

node default {
  exec { 'apt-get update':
    command => 'apt-get update',
    timeout => 60,
    tries   => 3
  }
  
  exec { 'apt_update':
    command => 'apt-get update',
    timeout => 60,
    tries   => 3
  }
  
  class { 'apt':
    force_aptget_update => true
  }
  
  package { ['python-software-properties']:
    ensure  => 'installed',
    require => Exec['apt-get update'],
    before => Package[['apache', 'php']]
  }
  
  package { ['build-essential', 'git', 'curl']:
    ensure  => 'installed',
    require => Exec['apt-get update'],
    before => Package[['apache', 'php']]
  }

  $vmconfig = parsejson(file('/vagrant/puppet/config/config.json'))
  $vmuserconfig = parsejson(file('/vagrant/puppet/config/vm.config.json'))
  
  file { '/var/www/public':
    ensure  => 'link',
    target  => '/vagrant/www',
    require => Package['apache'],
    notify  => Service['apache']
  }
  
  class { 'apache':
    port => '3000'
  }
  
  apache::module { 'rewrite':
    require => Package['apache']
  }
  
  #apache::listen { '8080':
  #  $namevirtualhost => false
  #}
  
  apache::vhost { 'public':
    docroot                   => '/var/www/public',
    server_name               => $vmuserconfig['hostname'],
    serveraliases             => concat(["my.dev"], $vmuserconfig['serveraliases']),
    server_admin              => $vmuserconfig['email'],
    directory                 => '/var/www/public',
    directory_options         => 'Indexes FollowSymLinks MultiViews',
    directory_allow_override  => 'All',
    template                  => '/vagrant/puppet/modules/apache/templates/virtualhost/vhost.conf.erb',
    require                   => Package[['apache']],
    env_variables             => concat($vmconfig['env_variables'], $vmuserconfig['env_variables']),
  }
  
  include apache::ssl
  
  class { 'php': }
  
  php::module { $vmuserconfig['php']['modules']:
    require => Package['php']
  }
  
  php::ini { 'php':
    value   => $vmuserconfig['php']['settings'],
    target  => 'php.ini',
    require => Package['php'],
    service => 'apache'
  }
  
  exec { 'usermod www-data':
    command => 'sudo usermod -a -G www-data vagrant'
  }
  
  exec { 'usermod sudo':
    command => 'sudo usermod -a -G sudo vagrant'
  }
  
  #class { 'postfix': }
  
  #class { "mysql":
  #  root_password => $vmuserconfig['mysql']['password'],
  #}
  
  #mysql::grant { $vmuserconfig['mysql']['database']:
  #  mysql_username            => $vmuserconfig['mysql']['user'],
  #  mysql_password            => $vmuserconfig['mysql']['password'],
  #  mysql_host                => '10.0.0.0/255.255.255.0',
  #  #mysql_db_init_query_file  => 'schema.sql'
  #}
  
  class { 'nginx': }
  
  nginx::resource::upstream { 'puppet_rack_app':
    members => [
      'localhost:3000',
    ],
  }
  
  nginx::resource::vhost { $vmuserconfig['nginx']['name']:
    proxy => 'http://puppet_rack_app',
    www_root => '/vagrant/www',
  }
  
  /*
  nginx::resource::vhost { $vmuserconfig['nginx']['name']:
    ensure      => present,
    name        => $vmuserconfig['nginx']['name'],
    www_root    => '/vagrant/www',
    listen_port => $vmuserconfig['nginx']['port']
  }
  */
  
  class { 'nodejs': }
}